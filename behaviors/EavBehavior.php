<?php

namespace mar\eav\behaviors;

use app\models\Doctor;
use mar\eav\models\EavAttribute;
use mar\eav\models\EavAttributeValue;
use yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;

class EavBehavior extends Behavior
{
    /** eav property type */
    /** simple text */
    const ATTRIBUTE_TYPE_TEXT = 1;
    /** will bet serialized before save and unserialized on __get($property) */
    //Temporary disabled
    const ATTRIBUTE_TYPE_ARRAY = 2;

    /**set empty if not set in Yii::$app->request */
    const MODE_SET_EMPTY_IF_NO_VALUE_IN_REQUEST = 1;

    /** if enable for class, can set any properties without declaration in behavior */
    const MODE_FREE_ATTRIBUTES = 2;


    /** @var ActiveRecord $this ->owner */
    /** @var  string - alias which will be used to identify model class in db */
    public $modelAlias;
    /** @var  array in forman ['name' => $label] */
    public $eavAttributesList;
    /** @var array mods for behavior */
    public $modes;

    /**
     * set Eav attribute by It's name
     * @param string $name
     * @param mixed $value
     * @return boolean
     * */
    public function setEavAttribute($name, $value)
    {
        /** @var EavAttribute $eavAttribute */
        if ($eavAttribute = $this->getEavAttributeModel($name, $this->modelAlias)) {
            if (is_array($value)) {
                $value = serialize($value);
            }
            $eavAttribute->setValue($this->getWorkModel(), $value);
        }
    }

    /**
     * get Eav attribute by It's name
     * @param string $name
     * @return mixed
     * */
    public function getEavAttribute($name)
    {
        /** @var EavAttribute $eavAttribute */
        if ($eavAttribute = $this->getEavAttributeModel($name, $this->modelAlias)) {
            $value = $eavAttribute->getValue($this->getWorkModel());
            if (@unserialize($value)) {
                $value = unserialize($value);
            }
            return $value;
        }
        return null;
    }

    /**
     * get eav attributes as array ['name' => 'value']
     * @return array
     * */
    public function getEavAttributes()
    {
        /** @var EavAttribute[] $attributesArray */
        $attributesArray = [];

        /** get eav attributes if they exists in db ( if free mode ) */
//        if ($this->mode(self::MODE_FREE_ATTRIBUTES)) {
        /** @var EavAttribute[] $attributes */
        $attributes = EavAttribute::find()->where([
            'alias' => $this->modelAlias,
        ])->all();
        foreach ($attributes as $key => $attribute) {
            $attributesArray[$attribute->name] = $attribute;
        }
//        }
        /**
         * change some attributes using behavior config and create if not exists in db
         */
        foreach ($this->eavAttributesList as $name => $value) {
            $attributesArray[$name] = $this->getEavAttributeModel($name, $this->modelAlias);
        }
        return $attributesArray;
    }

    /**
     * set eav attributes , input param should be array ['name' => 'value']
     * */
    public function setEavAttributes($values)
    {
        foreach ($values as $name => $value) {
            if ($this->eavAttributeExists($name)) {
                $this->setEavAttribute($name, $value);
            }
        }
    }


    /**
     * return eav attribute model for current model
     * @param string $name -attr name
     * @param string $alias - alias to  model::className
     * @param boolean $create -  create if not exists
     * @return EavAttribute
     * */
    protected function getEavAttributeModel($name, $alias, $create = true)
    {
        /** @var EavAttribute $attribute */
        $attribute = EavAttribute::find()->where([
            'alias' => $alias,
            'name' => $name,
        ])->one();

        if (!empty($attribute)) {
            return $attribute;
        } else if ($create) {
            /** @var EavAttribute $attribute */
            $attribute = new EavAttribute();
            $attribute->name = $name;
            $attribute->alias = $alias;
            //Todo:: make smth with labels
            $attribute->label = '';
            if ($attribute->save()) {
                return $attribute;
            }
        }
        return null;
    }

    /**
     * return return model which should has eav attributes ( change to $this->owner if use as a behavior)
     * @return ActiveRecord;
     * */
    public function getWorkModel()
    {
        return $this->owner;
    }

    public function attach($owner)
    {
        //clean old attributes which are not actual according current behavior config
        if (empty($this->modes[self::MODE_FREE_ATTRIBUTES])) {
            //Todo: what to do with old attributes, not used any more
//            $this->removeNotActualEavAttributes();
        }
        // to attach events
        parent::attach($owner);
        // add validation rules
        $validators = $owner->validators;
        foreach ($this->getEavAttributesRules() as $rule) {
            if ($rule instanceof yii\validators\Validator) {
                $validators->append($rule);
            } elseif (is_array($rule) && isset($rule[0], $rule[1])) { // attributes, validator type
                $validator = yii\validators\Validator::createValidator($rule[1], $owner, (array)$rule[0], array_slice($rule, 2));
                $validators->append($validator);
            } else {
                throw new yii\base\InvalidConfigException('Invalid validation rule: a rule must specify both attribute names and validator type.');
            }
        }
    }

    public function events()
    {
        //Todo: add attributes before search or something to find by eav attributes
        return [
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeSaveUpdates',
        ];
    }

    /**
     * something we shoud do after update or insert owner model
     * @param $event yii\base\Event
     * */
    public function beforeSaveUpdates($event)
    {
//        foreach ($this->getEavAttributes() as $name => $attribute) {
//            if ($this->mode(self::MODE_FREE_ATTRIBUTES) ||
//                !empty($this->eavAttributesList['name']['modes'])
//                && in_array(EavBehavior::MODE_SET_EMPTY_IF_NO_VALUE_IN_REQUEST,
//                    $this->eavAttributesList['name']['modes'])) {
//                $bodyParam = Yii::$app->getRequest()->getBodyParam($this->getScope());
//                if ($bodyParam && empty($bodyParam[$name])) {
//                    $this->setEavAttribute($name, '');
//                }
//            }
//        }
    }

    /** try to get scope ( form name ) which is used in $_REQEST while update owner model
     * @return string
     * */
    protected function getScope()
    {
        $scope = '';
        $ownerClassName = $this->owner->className();
        $ownerClassNameArr = explode('\\', $ownerClassName);
        if (is_array($ownerClassNameArr) && count($ownerClassNameArr)) {
            $scope = array_pop($ownerClassNameArr);
        }
        return $scope;
    }


    /**
     * PHP setter magic method.
     * This method is overridden so that AR attributes can be accessed like properties.
     * @param string $name property name
     * @param mixed $value property value
     */
    public function __set($name, $value)
    {
        if ($this->eavAttributeExists($name)) {
            $this->setEavAttribute($name, $value);
            return;
        }
        parent::__set($name, $value);
    }

    /**
     * PHP setter magic method.
     * This method is overridden so that AR attributes can be accessed like properties.
     * @param string $name property name
     * @param mixed $value property value
     */
    public function __get($name)
    {
        if ($this->eavAttributeExists($name)) {
            return $this->getEavAttribute($name);
        }
        return parent::__get($name);
    }


    public function attributes()
    {
        $names = parent::attributes(); // TODO: Change the autogenerated stubvs
        foreach ($this->getEavAttributes() as $name => $value) {
            $names[] = $name;
        }
        return $names;
    }

    public function canGetProperty($name, $checkVars = true)
    {
        if ($this->eavAttributeExists($name)) {
            return true;
        }
        return false;
    }

    public function canSetProperty($name, $checkVars = true)
    {
        if ($this->eavAttributeExists($name)) {
            return true;
        }
        return false;
    }

    /**
     * check if function exists in  eav attributes list>
     * @param string $name
     * @return boolean
     * */
    protected function eavAttributeExists($name)
    {
        return array_key_exists($name, $this->getEavAttributes());
    }

    /**
     * get array with rules for each eav attribute
     * @return array
     */
    public function getEavAttributesRules()
    {
        $rules = [];
        $attributeList = [];
//        if ($this->mode(self::MODE_FREE_ATTRIBUTES)) {
        foreach ($this->getEavAttributes() as $key => $attribute) {
            $attributeList[$attribute->name] = [];
        }
//        }

        $attributeList = array_merge($attributeList, $this->eavAttributesList);
        foreach ($attributeList as $name => $attrArr) {
            if (!empty($attrArr['rule']))
                $rules[] = $attrArr['rule'];
            else
                $rules[] = [$name, 'safe'];
        }

        return $rules;
    }


    /** return id of model to bind eav attribute value */
    protected function getObjectId()
    {
        return $this->id;
    }

    /** check if attribute declared as array type
     * @param string $name
     * @return boolean
     * */
    protected function isArrayType($name)
    {
        if (!empty($this->eavAttributesList[$name]['type'])
            && $this->eavAttributesList[$name]['type'] === EavBehavior::ATTRIBUTE_TYPE_ARRAY
        ) {
            return true;
        }
        return false;
    }

    /** remove from db all eav attributes for this model whick are mot actual ( not in     public $eavAttributesList ) */
    protected function  removeNotActualEavAttributes()
    {
        $eavAttributes = array_keys($this->eavAttributesList);
        //$eavAttributes = ['one', 'two'];
        /** @var EavAttribute[] $notActualAttributes */
        $notActualAttributes = EavAttribute::find()->where([
            'alias' => $this->modelAlias,
        ])->andWhere(['not in', 'name', $eavAttributes])->all();

        foreach ($notActualAttributes as $key => $attribute) {
            $attribute->delete();
        }
    }

    /**
     * check if global behavior mod is enabled
     * @param integer $mode
     * @return boolean
     * */
    protected function mode($mode)
    {
        return is_array($this->modes) && in_array($mode, $this->modes);
    }

    /** remove eav attribute */
    protected function removeEavAttribute()
    {
        //Todo: implement
    }

    /** check if ANY models of current class has attribute */
    public static function hasCreatedEavAttribute($name, $alias)
    {
        /** @var EavAttribute $attribute */
        return EavAttribute::find()->where([
            'alias' => $alias,
            'name' => $name,
        ])->one();
    }

    /**
     * find models or ids list foor eav attributes
     * @param array $params
     * @param boolean $idsOnly
     * @return ActiveRecord[]/array
     * */
    public function findByEavAttributes($params, $idsOnly = false)
    {
        /** @var yii\db\ActiveQuery $query */
        $query = EavAttributeValue::find()->alias('outer_tbl')->where([]);
        /** @var ActiveRecord[] $models */
        $models = [];
        foreach ($params as $name => $value) {
            /** @var EavAttribute $eavAtrribute */
            $eavAtrribute = $this->getEavAttributeModel($name, $this->modelAlias);
            if ($eavAtrribute) {
                $subQuery = EavAttributeValue::find()->alias('inner_tbl')->where([
                    'inner_tbl.attribute_id' => $eavAtrribute->id,
                ])->andWhere('outer_tbl.object_id = inner_tbl.object_id');

                if (is_array($value) && !empty($value['min']) && !empty($value['max'])) {
                    $subQuery->andWhere(['>=', 'inner_tbl.value', intval($value['min'])]);
                    $subQuery->andWhere(['<=', 'inner_tbl.value', intval($value['max'])]);
                } else {
                    $subQuery->andWhere(['like', 'inner_tbl.value', $value]);
                }

                $query->andWhere(
                    ['exists', $subQuery]
                );

                $eavAtrributeValues = $query->asArray()->all();
                $modelsIds = array_column($eavAtrributeValues, 'object_id');
                $modelsIds = array_unique($modelsIds);
                if ($idsOnly) {
                    return $modelsIds;
                }
                $className = $this->getWorkModel()->className();

                /** @var yii\db\ActiveRecord $className */
                $models = $className::find()->where(['id' => $modelsIds])->all();
            }
        }
        return $models;
    }
}